Outils LDAP pour l'institu Curie
--------------------------------

Pour convertir des exports LDIF AD ou SUN:

    curie2supann ad.ldif sun.ldif >supann.ldif

Ne sont exportés que les entrées pour lesquelles des enregistrements coté AD et
SUN sont trouvés.
